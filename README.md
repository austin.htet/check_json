# Description
Icinga/Nagios, and compatible, plugin to obtain JSON data and perform comparisons/check against a specific key/subkey.

* Get timestamp and determine difference from current datetime
* Use regex and match/compare string data
* Check integers against a defined range

Please reference [this guide](https://nagios-plugins.org/doc/guidelines.html#THRESHOLDFORMAT) when setting -w and -c ranges.

# Install
* `git clone https://gitlab.com/Jedimaster0/check_json.git`
* `cd check_json`
* `pip3 install -e .`
* Create a symbolic link to check_json.py in your `/usr/local/nagios/libexec`, or wherever your `$USER1$` path lives.

# Example
```
python .\check_json.py -u https://api.storj.io/contacts/15cc7dfe5443f6302fe65bf5503f7c34a1bdc53b -k lastSeen -td minutes -w 5 -c 10
CHECKJSON OK - timediff is 0 | timediff=0.0;5;10;0

python .\check_json.py -u https://api.storj.io/contacts/15cc7dfe5443f6302fe65bf5503f7c34a1bdc53b -k address -m '^storj.mimir-tech.org$'
CHECKJSON OK - match is True | match=True;1:;1:

python .\check_json.py -u https://api.storj.io/contacts/15cc7dfe5443f6302fe65bf5503f7c34a1bdc53b -k reputation -w '4800:' -c '4500:'
CHECKJSON OK - intiger is 5000 | intiger=5000;4800:;4500:
```

# Usage
```
python .\check_json.py --help
usage: check_json.py [-h] [-u URL] [-j JSON] -k KEY [-f FUNCTION] [-R REGEX]
                     [-tf TIMEFORMAT] [-td TIMEDURATION] [-tz TIMEZONE]
                     [-m MATCH] [-w WARNING] [-c CRITICAL] [-v] [-D]

optional arguments:
  -h, --help            show this help message and exit
  -u URL, --url URL     URL to obtain JSON data.
  -j JSON, --json JSON  If not specifying a URL, you can specify the JSON
                        string directly.
  -k KEY, --key KEY     json key(s) using "." delimination.
  -f FUNCTION, --function FUNCTION
                        Function comparison to be performed
                        (timediff|match|integer). Defaults to "auto"
  -R REGEX, --regex REGEX
                        regex pattern for pulling timestamp from JSON. Should
                        be the regex equivalent of timeformat. Defaults to
                        yyyy-mm-ddTHH:MM:SS
  -tf TIMEFORMAT, --timeformat TIMEFORMAT
                        Time format pattern:
                        (https://pendulum.eustace.io/docs/#formatter)
  -td TIMEDURATION, --timeduration TIMEDURATION
                        Output format of timediff
                        (days|hours|minutes|seconds). Default is seconds.
  -tz TIMEZONE, --timezone TIMEZONE
                        IANA time zones (https://en.wikipedia.org/wiki/List_of
                        _tz_database_time_zones#List). Default is Etc/UTC
  -m MATCH, --match MATCH
                        string or regex pattern for comparison against the
                        JSON key value
  -w WARNING, --warning WARNING
  -c CRITICAL, --critical CRITICAL
  -v, --verbose
  -D, --debug
  ```